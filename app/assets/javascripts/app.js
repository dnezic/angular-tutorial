
dependencies = [
    'ngRoute',
    'ngResource',
    'ui.bootstrap',
    'myApp.users',
    'myApp.common',
    'myApp.filters',
    'myApp.directives'
]

app = angular.module('myApp', dependencies)

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/', {
               templateUrl: '/assets/partials/hello.html'
            })
      .when('/controller', {
               templateUrl: '/assets/partials/controller.html'
            })
      .when('/underscore', {
               templateUrl: '/assets/partials/underscore.html'
            })
      .when('/nested1', {
               templateUrl: '/assets/partials/nested1.html'
            })
      .when('/nested2', {
               templateUrl: '/assets/partials/nested2.html'
            })   
      .when('/nested3', {
               templateUrl: '/assets/partials/nested3.html'
            })         
      .when('/transclude', {
               templateUrl: '/assets/partials/transclude.html'
            }) 
      .when('/nested4', {
               templateUrl: '/assets/partials/nested4.html'
            })            
      .when('/filters', {
               templateUrl: '/assets/partials/filters.html'
            })            
      .when('/users/view', {
               templateUrl: '/assets/partials/view.html'
            })
      .when('/users/create', {
               templateUrl: '/assets/partials/create.html'
            })
      .otherwise({
        redirectTo: '/'
      });
  }]);

app.config(['$logProvider', function($logProvider){
    $logProvider.debugEnabled(true);
}]);



/* configuration of underscore library */
var underscore = angular.module('underscore', []);
underscore.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});  

/* configuration of underscore.string library */
_.mixin(_.string.exports());