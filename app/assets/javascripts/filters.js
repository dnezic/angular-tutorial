angular.module('myApp.filters', []).filter('camelCase', function() {
return function(input) {
  return _.capitalize(input);
};
});