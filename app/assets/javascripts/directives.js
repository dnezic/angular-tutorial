var directives = angular.module('myApp.directives', []);

directives.directive('myBorder', [function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: false,
        template: '<div style="border: 1px dashed #000" ng-transclude></div>'		
    };	
    }]);


directives.directive('person', [function () {
    return {
        restrict: 'E',
        transclude: false,
        /* try without new scope. */
        scope: {

        },
        template: '<p><b>Person: {{person.firstName}}</b></p>',        
		controller: ['$log','$scope', (function($log, $scope) {        	
			$scope.person = $scope.$parent.person;
        })]        
    };	
    }]);

directives.directive('persons', [function () {
    return {
        restrict: 'E',
        scope: {
        	items: '=items'
        },
        transclude: true,
        template: '<ul><li ng-repeat="item in items"><div trans-x>{{item}} is transcluded</div></li></ul>',        
		controller: ['$log','$scope', (function($log, $scope) {        	
			
        })]        
    };
    }]);

directives.directive('personInside', [function () {
    return {
    	require: '^persons',
        restrict: 'E',  
        scope: {},
        transclude: false,  
        template: '<p><b>Person inside: {{$parent.item.firstName}}</b></p>',     
        link: function(scope, element, attrs, personsCtrl) {
        	console.log('scope: ', scope);
        }   
    };	
    }]);	
   
directives.directive('transX', function() {
  return {
    link: {
      pre: function(scope, element, attr, ctrl, transclude) {
        if (transclude) {
          transclude(scope, function(clone) {
            element.replaceWith(clone);
          });
        }
      }
    }
  };
});