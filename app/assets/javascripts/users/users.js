/* module */
var usersModule = angular.module('myApp.users', []);

/* services */
usersModule.factory('User', ['$resource',
		function($resource){
			return $resource('users/:pk', {pk:'@pk'}, {
				query: {method:'GET', isArray:true}                                               
			});
}]);

usersModule.controller('ListUsersCtrl',
		[ '$scope', '$log', 'User', function($scope, $log, User) {

			$scope.users = User.query();

}]);

usersModule.controller('CreateUserCtrl',
		[ '$scope', '$log', '$location', 'User', function($scope, $log, $location, User) {

			$scope.user = {
				active: true
			};

			$scope.createUser = function() {
				User.save($scope.user).$promise.then(function() {
					$location.path('/users/view');
				});
			};

}]);
