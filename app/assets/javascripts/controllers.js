
var commonModule = angular.module('myApp.common', []);

commonCtrl = commonModule.controller('CommonCtrl',
		[ '$scope', '$log', function($scope, $log) {			
			$log.debug('myApp.common::CommonCtrl:Initializing Common Controller. ');
			$scope.data = {
				IQ: 22
			};

			$scope.persons = [
                    {firstName: 'moe', lastName: 'moeić', age: 40},
                    {firstName: 'miki', lastName: 'mikić', age: 40},
                    {firstName: 'larry', lastName: 'larryić', age: 50},
                    {firstName: 'curly', lastName: 'curlyić', age: 60}
                ];
			$scope.personsNames = _.pluck($scope.persons, 'firstName');			
			$scope.groupedByAge = _.groupBy($scope.persons, 'age');	

		}]);

commonAsCtrl = commonModule.controller('CommonAsCtrl',
		[ '$log', function($log) {			
			$log.debug('myApp.common::CommonAsCtrl:Initializing CommonAs Controller. ');
			this.data = {
				IQ: 22
			};

		}]);

nestedCtrl = commonModule.controller('NestedCtrl',
		[ '$scope', '$log', function($scope, $log) {			
			$log.debug('myApp.common::NestedCtrl:Initializing NestedCtrl Controller. ');
			
			$scope.persons = [
                {firstName:'miki'},{firstName:'georgije'}
            ]
			$scope.commonObj = {text:'Common object with text'};
			$scope.commonTxt = 'Common text for all';

		}]);

personCtrl = commonModule.controller('PersonCtrl',
		[ '$scope', '$log', function($scope, $log) {			
			$log.debug('myApp.common::PersonCtrl:Initializing PersonCtrl Controller. ');
			
			$scope.draw = function(person) {
				return 'Drawing: ' + person.firstName;
			};

			$scope.drawPersons = function(persons) {
				return 'Drawing persons: ' + _.join(",", _.pluck(persons,'firstName'));
			};

		}]);

personCtrl = commonModule.controller('PersonCtrl',
        [ '$scope', '$log', function($scope, $log) {            
            $log.debug('myApp.common::PersonCtrl:Initializing PersonCtrl Controller. ');
            
            $scope.draw = function(person) {
                return 'Drawing: ' + person.firstName;
            };

            $scope.drawPersons = function(persons) {
                return 'Drawing persons: ' + _.join(",", _.pluck(persons,'firstName'));
            };

        }]);



treeCtrl = commonModule.controller('TreeCtrl',
        [ '$scope', '$log', function($scope, $log) {            
            $log.debug('myApp.common::TreeCtrl:Initializing TreeCtrl Controller. ');
            
            $scope.tree = {
                name: 'Root',
                children: [
                    {
                        name: 'Child 1',
                        children: [
                            { 
                                name: 'Sub Child 1',
                                children: []
                            }
                        ]
                    },
                    {
                        name: 'Child 2',
                        children: [
                            {
                                name: 'Sub Child 2',
                                children: []
                            }
                        ]
                    }
                ]
            }
            
        }]);
